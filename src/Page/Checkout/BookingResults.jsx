import React, { Fragment } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { USER_LOGIN } from '../../Redux/Constants/UserLogin'
// import { SET_USER } from '../../Redux/Constants/ConstantsUser'
import '../../assets/styles/Checkout.css'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { datVeAction, quanLiDatVeAction } from '../../Redux/Actions/QuanLiDatVeAction'
import { CloseOutlined } from '@ant-design/icons'
import { DAT_VE } from '../../Redux/Constants/QuanLiDatVe'
import _ from 'lodash'
import { getInfoUserAction } from '../../Redux/Actions/GetInforUser'
import moment from 'moment/moment'

export default function BookingResults() {
    const dispatch = useDispatch();
    const { thongTinNguoiDung } = useSelector(state => state.userReducer);
    console.log('thongTinNguoiDung: ', thongTinNguoiDung);
    useEffect(() => {
        const action = getInfoUserAction();
        dispatch(action);
    }, []);


    const renderTicketItem = () => {
        return thongTinNguoiDung.thongTinDatVe?.map((ticket, index) => {
            return (
                <div key={index} className="p-2 lg:w-1/3 md:w-1/2 w-full">
                    <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                        <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src={ticket.hinhAnh} />
                        <div className="flex-grow">
                            <h2 className="text-red-700 title-font font-xl text-xl">Tên Phim: {ticket.tenPhim}</h2>
                            <p className="text-black">Ngày Đặt: {moment(ticket.ngayDat).format('DD-MM-YYYY | hh:mm')}</p>
                            <p className="text-black">Thời Lượng Phim: {ticket.thoiLuongPhim} phút, Giá Vé: {ticket.giaVe.toLocaleString()}</p>
                            {ticket.danhSachGhe.slice(0, 1).map((dsg, index) => {
                                return <div key={index} >
                                    <h2 className="text-green-700 title-font font-xl text-xl">{dsg.tenHeThongRap}</h2>
                                    <p key={index} className="text-black">{dsg.tenRap} , Ghế Số: {ticket.danhSachGhe.map((ghe, index) => {
                                        return <span className=' ml-2 ' key={index} > {ghe.tenGhe}</span>
                                    })}</p>
                                </div>
                            })}
                        </div>
                    </div>
                </div >
            );
        })
    };
    return (
        <div>

            <div className="p-5">
                <section className="text-gray-600 body-font">
                    <div className="container px-5 py-24 mx-auto" style={{ marginTop: 50 }}>
                        <div className="flex flex-col text-center w-full mb-20">
                            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-purple-700">
                                Lịch sử đặt vé
                            </h1>
                            <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                                Hãy kiểm tra thông tin địa điểm và thời gian để xem phim vui vẻ
                                bạn nhé
                            </p>
                            <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                                Vui Lòng Chờ Trong Giây Lát...
                            </p>
                        </div>
                        <div className="flex flex-wrap -m-2">{renderTicketItem()}</div>
                    </div>
                </section>
            </div>
        </div>

    );
}
