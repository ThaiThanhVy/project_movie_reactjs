import React, { Fragment } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import '../../assets/styles/Checkout.css'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { datVeAction, quanLiDatVeAction } from '../../Redux/Actions/QuanLiDatVeAction'
import { CloseOutlined } from '@ant-design/icons'
import { DAT_VE } from '../../Redux/Constants/QuanLiDatVe'
import _ from 'lodash'
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe"




export default function Checkout(props) {

    const { chiTietPhongVe, danhSachGheDangDat } = useSelector(state => state.quanLiDatVeReducer)
    const { userInfor } = useSelector(state => state.userReducer)

    let dispatch = useDispatch()

    let { id } = useParams()

    useEffect(() => {
        dispatch(quanLiDatVeAction(id))
    }, [])

    let { thongTinPhim, danhSachGhe } = chiTietPhongVe;

    const renderDanhSachGhe = () => {
        return danhSachGhe?.map((ghe, index) => {

            let gheVip = ghe.loaiGhe === 'Vip' ? 'gheVip' : '';
            let gheDaDat = ghe.daDat === true ? 'gheDuocChon' : '';

            let gheDangDat = '';

            let indexGheDangDat = danhSachGheDangDat.findIndex(gheDangDat => gheDangDat.maGhe === ghe.maGhe)

            let classGheDaDuocDat = '';
            if (userInfor.taiKhoan === ghe.taiKhoanNguoiDat) {
                classGheDaDuocDat = 'gheDaDuocChon'
            }

            if (indexGheDangDat !== -1) {
                gheDangDat = 'gheDangChon'
            }

            return <Fragment key={index}>
                <button onClick={() => {
                    dispatch({
                        type: DAT_VE,
                        ghe
                    })
                }} disabled={ghe.daDat} className={`ghe ${classGheDaDuocDat} ${gheVip} ${gheDaDat} ${gheDangDat} text-center`} key={index}>

                    {ghe.daDat ? <CloseOutlined style={{ verticalAlign: 'middle' }} /> : ghe.stt}
                </button>

                {(index + 1) % 16 === 0 ? <br /> : ''}
            </Fragment >
        })
    }


    return (
        <div style={{ paddingTop: 79 }}>
            <div className='grid grid-cols-12'>
                <div className='col-span-8'>
                    {/* <div className='screen'></div>
                    <div className='text-white text-center'>Man hinh</div> */}
                    <div className='text-center table_res'>
                        {renderDanhSachGhe()}
                    </div>
                    <div className='flex justify-center items-center'>
                        <button className='gheCss gheVip'></button>
                        <br />
                        <span className='text-gray-700'>Ghế Vip</span>
                        <button className='gheCss gheDuocChon'><CloseOutlined style={{ verticalAlign: 'middle' }} /></button>
                        <br />
                        <span className='text-gray-700'>Ghế Đã Đặt</span>
                        <button className='gheCss gheDangChon'></button>
                        <br />
                        <span className='text-gray-700'>Ghế Đang Đặt</span>
                        <button className='gheCss gheChuaDat'></button>
                        <br />
                        <span className='text-gray-700'>Ghế Chưa Đặt</span>
                    </div>
                </div>
                <div className='col-span-4' style={{ height: '75%' }}>
                    <h3 className='text-center text-2xl text-green-400 mt-4 mb-4'>{
                        danhSachGheDangDat.reduce((tongTien, ghe, index) => {
                            return tongTien += ghe.giaVe
                        }, 0).toLocaleString()
                    } VND</h3>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Cụm Rạp:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenCumRap}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Địa chỉ:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.diaChi}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Rạp:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenCumRap}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Ngày giờ chiếu:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.ngayChieu} ~ {thongTinPhim?.gioChieu}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Tên Phim:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenPhim}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Chọn Ghế:</h3>
                        <h3>
                            {_.sortBy(danhSachGheDangDat, ['stt'])?.map((gheDangDat, index) => {
                                return <span key={index} className='ml-2 text-green-500 text-base font-bold'>
                                    <span> {gheDangDat.stt}</span>
                                </span>
                            })}
                        </h3>
                    </div>
                    <hr />
                    {/* <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Hình Ảnh</h3>
                        <img className='text-green-500 text-base font-bold text-right w-24 h-40 object-cover mr-4' src={thongTinPhim?.hinhAnh} alt="" />
                    </div> */}
                    <hr />
                    <div className='flex justify-center w-full h-16 bg-orange-600 items-center'>
                        <div onClick={() => {
                            let thongTinDatVe = new ThongTinDatVe
                            thongTinDatVe.danhSachVe = danhSachGheDangDat
                            thongTinDatVe.maLichChieu = thongTinPhim.maLichChieu
                            dispatch(datVeAction(thongTinDatVe))
                        }} className='text-light text-3xl cursor-pointer ' data-toggle="modal" data-target="#exampleModalCenter" >
                            Đặt Vé
                        </div>
                    </div>
                    <div className="modal modal_checkout fade" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4 className="modal-title" id="exampleModalLongTitle">Đặt Vé</h4>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <h1 className='text-red-500 text-center text-3xl '>Chúc Mừng Bạn Đã Đặt Vé Thành Công</h1>
                                    <h3 className='text-black text-xl text-center'>Vui Lòng Kiểm Tra Trong Lịch Sử Đặt Vé</h3>
                                </div>
                                <div className="modal-footer">
                                    <button className="btn btn-secondary" data-dismiss="modal">Đồng Ý</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
